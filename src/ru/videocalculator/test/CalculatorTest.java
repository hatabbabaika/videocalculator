/**
 * 
 */
package ru.videocalculator.test;

import junit.framework.TestCase;
import java.util.LinkedList;
import ru.videocalculator.main.*;
import java.time.Duration;

/**
 * @author hatab
 *
 */
public class CalculatorTest extends TestCase {
	
	private MemoryView expected = new MemoryView();
	private VideoFlow flow = new VideoFlow();
	
	private MemoryView expected1 = new MemoryView();
	private VideoFlow flow1 = new VideoFlow();
	
	private MemoryView expected2 = new MemoryView();
	private VideoFlow flow2 = new VideoFlow();
	
	private MemoryView expected3 = new MemoryView();
	private VideoFlow flow3 = new VideoFlow();

	/**
	 * @throws java.lang.Exception
	 */
	
	protected void setUp() throws Exception {
		expected
			.setMB(109_350_000)
			.setTB(104);			
		flow
			.setSize(3840, 2160)
			.setColorDepth(256)
			.setFramerate(60);
		
		expected1
			.setKB(218_700_000);			
		flow1
			.setSize(360, 240)
			.setColorDepth(32)
			.setFramerate(60);
		
		expected2
			.setMB(2_531_250)
			.setGB(2_471)
			.setTB(2);			
		flow2
			.setSize(1280, 720)
			.setColorDepth(64)
			.setFramerate(24);
		
		expected3
			.setMB(57_408_750)
			.setGB(56_063)
			.setTB(54);
		flow3
			.setSize(3840, 2160)
			.setColorDepth(48)
			.setFramerate(48);
	}

	/**
	 * Test method for {@link ru.videocalculator.main.Calculator#sum(int, int)}.
	 */
	public void testSum() {
		
		LinkedList<Video> videos = new LinkedList<>();
		videos.add(new Video(flow, Duration.ofSeconds(7200)));
		
		Calculator calc = new Calculator(videos);
		assertEquals(expected, calc.calculate());
	}
	
	public void testSum1() {
		LinkedList<Video> videos = new LinkedList<>();
		videos.add(new Video(flow1, Duration.ofSeconds(10800)));
		
		Calculator calc = new Calculator(videos);
		assertEquals(expected1, calc.calculate());
	}
	
	public void testSum2() {
		LinkedList<Video> videos = new LinkedList<>();
		videos.add(new Video(flow2, Duration.ofSeconds(15000)));
		
		Calculator calc = new Calculator(videos);
		assertEquals(expected2, calc.calculate());
	}
	
	public void testSum3() {
		LinkedList<Video> videos = new LinkedList<>();
		videos.add(new Video(flow3, Duration.ofSeconds(25_200)));
		
		Calculator calc = new Calculator(videos);
		assertEquals(expected3, calc.calculate());
	}

}
