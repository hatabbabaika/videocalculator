package ru.videocalculator.main;

import java.time.Duration;

public class Video {
	
	private VideoFlow flow;
	
	private Duration duration = Duration.ZERO;

	public Video(VideoFlow flow, Duration duration) {
		this.flow = flow;
		this.duration = duration;
	}
	
	public MemoryView getSize() {
		return this.flow.getBitrate()
				.multiply(this.duration.toSeconds());
	}
	
	public void setFlow(VideoFlow flow) {
		this.flow = flow;
	}
	
	public int getVideoFlowHeight() {
		return this.flow.getHeight();
	}
	
	public int getVideoFlowWidth() {
		return this.flow.getWidth();
	}
	
	public long getDuration() {
		return this.duration.toSeconds();
	}
	
	public void setDuration(Duration duration) throws Exception {
		if (duration.isNegative()) {
			throw new Exception("Duration is uncorrect");
		}
		this.duration = duration;
	}
	
	public String toString() {
		return String.format(
				"%s %d:%02d:%02d",
				this.flow.toString(),
				this.duration.toHours(),
				this.duration.toMinutesPart(),
				this.duration.toSecondsPart()
			);
	}

}
