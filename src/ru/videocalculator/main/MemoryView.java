package ru.videocalculator.main;

import java.util.LinkedList;

public class MemoryView {
	
	public enum Sizes {
		b,
		B,
		KB,
		MB,
		GB,
		TB,
		PB,
		EB,
		ZB,
		YB
	}

	LinkedList<Integer> values = new LinkedList<>();
	
	public MemoryView(int ... values) {
		if (values.length > 0) {
			this.values = MemoryView.toLinkedList(values);
			this.optimeze();
		}
	}

	public MemoryView add(MemoryView toAdd) {
		for (int i = 0; i < this.values.size(); i++) {
			this.values.set(i, (int) (this.values.get(i) + toAdd.values.get(i)));
		}
		for (int i = this.values.size(); i < toAdd.values.size(); i++) {
			this.values.add(toAdd.values.get(i));
		}
		
		this.optimeze();
		return this;
	}
	
	public MemoryView multiply(long multiplier) {
		for (int i = 0; i < this.values.size(); i++) {
			this.values.set(i, (int) (this.values.get(i) * multiplier));
		}
		this.optimeze();
		return this;
	}
	
	public MemoryView setTo(Sizes pos, int value) {
		int intPos = pos.ordinal();
		while (intPos >= this.values.size()) {
			this.values.add(0);
		}

		this.values.set(intPos, value);
		this.optimeze();
		return this;
	}
	
	public String toUserView() {
		int lastIndex = this.values.size() - 1; 
		float value = (float) this.values.get(lastIndex);
		if (lastIndex > 0) {
			value += this.values.get(lastIndex - 1).floatValue() / 1024;
		}
		return String.format("%.1f %s", value, MemoryView.Sizes.values()[lastIndex].name());
	}
	
	@Override
	public String toString() {
		return this.values.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		try {
			return this.values.equals(((MemoryView)obj).values);
		}
		catch (Exception e) {
			return false;
		}
	}

	private void optimeze() {
		MemoryView.optimeze(this.values);
	}
	
	public MemoryView setb(int value) {
		return this.setTo(MemoryView.Sizes.b, value);
	}
	
	public MemoryView setB(int value) {
		return this.setTo(MemoryView.Sizes.B, value);
	}
	
	public MemoryView setKB(int value) {
		return this.setTo(MemoryView.Sizes.KB, value);
	}
	
	public MemoryView setMB(int value) {
		return this.setTo(MemoryView.Sizes.MB, value);
	}
	
	public MemoryView setGB(int value) {
		return this.setTo(MemoryView.Sizes.GB, value);
	}
	
	public MemoryView setTB(int value) {
		return this.setTo(MemoryView.Sizes.TB, value);
	}
	
	public MemoryView setPB(int value) {
		return this.setTo(MemoryView.Sizes.PB, value);
	}
	
	public MemoryView setEB(int value) {
		return this.setTo(MemoryView.Sizes.EB, value);
	}
	
	public MemoryView setZB(int value) {
		return this.setTo(MemoryView.Sizes.ZB, value);
	}
	
	public MemoryView setYB(int value) {
		return this.setTo(MemoryView.Sizes.YB, value);
	}

	private static void optimeze(LinkedList<Integer> values) {
		int toAppend = 0;
		for (int i = 0; i < values.size(); i++) {
			int value = values.get(i);
			value += toAppend;
			
			int divider = (i > 0) ? 1024 : 8;
			toAppend = (int) (value / divider);
			value %= divider;
			values.set(i, value);
			if (i == values.size() - 1 && toAppend > 0) {
				values.add(0);
			}
		}
	}
	
	private static LinkedList<Integer> toLinkedList(int[] array) {
		LinkedList<Integer> result = new LinkedList<>();

		for (int i = array.length - 1; i >= 0; i--) {
			result.add(array[i]);
		}
		
		return result;
	}
}
