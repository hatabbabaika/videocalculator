package ru.videocalculator.main;

import java.util.LinkedList;

public class Calculator {
	
	private LinkedList<Video> videos;
	
	public Calculator(LinkedList<Video> videos) {
		this.videos = videos;
	}
	
	public MemoryView calculate() {
		MemoryView sum = new MemoryView();
		for (Video video : this.videos) {
			sum.add(video.getSize());
		}
		return sum;
	}
	
	public void setVideos(LinkedList<Video> videos) {
		this.videos = videos;
	}
}
