package ru.videocalculator.main;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.Duration;
import java.util.LinkedList;

import javax.swing.JTextArea;

public class Application {
	private JFrame frame;
	private JTextField widthTextField;
	private JTextField heightTextField;
	private JTextField durationTextField;
	private LinkedList<Video> videos = new LinkedList<Video>();
	DefaultListModel<String> model = new DefaultListModel<String>(); 
	private int currentVideoIndex = -1;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application window = new Application();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Application() {
		initialize();
	}
	
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("������ ������ ������ ��� �������� ����� � ����������");
		frame.setBounds(100, 100, 557, 599);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		widthTextField = new JTextField();
		widthTextField.setBounds(27, 48, 210, 19);
		frame.getContentPane().add(widthTextField);
		widthTextField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("������ �����");
		lblNewLabel.setBounds(27, 25, 210, 13);
		frame.getContentPane().add(lblNewLabel);
		
		heightTextField = new JTextField();
		heightTextField.setColumns(10);
		heightTextField.setBounds(25, 100, 212, 19);
		frame.getContentPane().add(heightTextField);
		
		JLabel lblNewLabel_1 = new JLabel("������ �����");
		lblNewLabel_1.setBounds(25, 77, 212, 13);
		frame.getContentPane().add(lblNewLabel_1);
		
		JButton add = new JButton("��������");
		add.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					int height = Integer.parseInt(heightTextField.getText());
					if (height < 0) {
						throw new Exception("������ �� ����� ���� �������������");
					}
					
					int width = Integer.parseInt(widthTextField.getText());
					if (width < 0) {
						throw new Exception("������ �� ����� ���� �������������");
					}
					
					long seconds = Integer.parseInt(durationTextField.getText());
					if (seconds < 0) {
						throw new Exception("������������ �� ����� ���� �������������");
					}
					Duration duration = Duration.ofSeconds(seconds);
					
					VideoFlow vf = new VideoFlow(width, height);
					Video v = new Video(vf, duration);
					videos.add(v);
					model.addElement(v.toString());
				} catch (NumberFormatException err) {
					JOptionPane.showMessageDialog(frame, "��������� ��������� �� �� ������� ������?");
				} catch (Exception err) {
					JOptionPane.showMessageDialog(frame, err.toString());
				}
			}
		});
		add.setBounds(27, 181, 109, 21);
		frame.getContentPane().add(add);
		
		durationTextField = new JTextField();
		durationTextField.setColumns(10);
		durationTextField.setBounds(27, 152, 210, 19);
		frame.getContentPane().add(durationTextField);
		
		JLabel lblNewLabel_1_1 = new JLabel("������������ ����� � ��������");
		lblNewLabel_1_1.setBounds(27, 129, 210, 13);
		frame.getContentPane().add(lblNewLabel_1_1);
		
		JList<String> formatlist = new JList<String>();
		formatlist.getSelectionModel().addListSelectionListener(e -> {
			int index = formatlist.getSelectedIndex();
			int height;
			int width;
			switch (index) {
				case 0:
					height = 2160;
					width = 3840;
					break;
				case 1:
					height = 1080;
					width = 1920;
					break;
				case 2: 
					height = 720;
					width = 1280;
					break;
				case 3: 
					height = 480;
					width = 640;
					break;
				case 4: 
					height = 640;
					width = 360;
					break;
				case 5: 
					height = 240;
					width = 320;
					break;
				default: return;
			}
			heightTextField.setText(String.valueOf(height));
			widthTextField.setText(String.valueOf(width));
		});
		formatlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		DefaultListModel<String> formatlistModel = new DefaultListModel<String>();
		String[] values = new String[] {"4K", "1080p", "720p", "480p", "360p", "240p"};
		for (String str : values) {
			formatlistModel.addElement(str);
		}
		formatlist.setModel(formatlistModel);
		formatlist.setBounds(247, 50, 152, 127);
		frame.getContentPane().add(formatlist);
		
		JList<String> videosList = new JList<String>();
		videosList.setModel(model);
		videosList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		videosList.setBounds(27, 245, 210, 121);
		videosList.getSelectionModel().addListSelectionListener(e -> {
			int index = videosList.getSelectedIndex();
			if (index == -1 || index >= model.size() || index >= videos.size()) {
				return;
			}
			Video selectedVideo = videos.get(index);
			int height = selectedVideo.getVideoFlowHeight();
			int width = selectedVideo.getVideoFlowWidth();
			long seconds = selectedVideo.getDuration();
			heightTextField.setText(String.valueOf(height));
			widthTextField.setText(String.valueOf(width));
			durationTextField.setText(String.valueOf(seconds));
			currentVideoIndex = index;
		});
		frame.getContentPane().add(videosList);
		
		JButton remove = new JButton("�������");
		remove.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int index = currentVideoIndex;
				if (index == -1 || index >= videos.size() || index >= model.size()) {
					return;
				}
				videosList.clearSelection();
				videos.remove(index);
				currentVideoIndex = -1;
				model.remove(index);
			}
		});
		remove.setBounds(247, 242, 85, 21);
		frame.getContentPane().add(remove);
		
		JButton change = new JButton("��������");
		change.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int index = currentVideoIndex;
				try {
					if (index == -1) {
						throw new Exception("�������� ������� �� ������, ��� ��� ���������");
					}
					
					int height = Integer.parseInt(heightTextField.getText());
					if (height < 0) {
						throw new Exception("������ �� ����� ���� �������������");
					}
					
					int width = Integer.parseInt(widthTextField.getText());
					if (width < 0) {
						throw new Exception("������ �� ����� ���� �������������");
					}
					
					long seconds = Integer.parseInt(durationTextField.getText());
					if (seconds < 0) {
						throw new Exception("������������ �� ����� ���� �������������");
					}
					Duration duration = Duration.ofSeconds(seconds);
						
					VideoFlow vf = new VideoFlow(width, height);
					Video v = new Video(vf, duration);
					videos.set(index, v);
					model.set(index, v.toString());
				} catch (NumberFormatException err) {
					JOptionPane.showMessageDialog(frame, "��������� ��������� �� �� ������� ������?");
				} catch (Exception err) {
					JOptionPane.showMessageDialog(frame, err.toString());
				}
			}
		});
		change.setBounds(146, 181, 91, 21);
		frame.getContentPane().add(change);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(27, 407, 210, 108);
		frame.getContentPane().add(textArea);
		
		JButton calculate = new JButton("��������� ����� ������");
		calculate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (videos.size() > 0) {
					Calculator calc = new Calculator(videos);
					MemoryView mv = calc.calculate();
					textArea.setText(mv.toUserView());
				} else {
					JOptionPane.showMessageDialog(frame, "�������� �����������");
				}
			}
		});
		calculate.setBounds(26, 376, 211, 21);
		frame.getContentPane().add(calculate);
		
	}
}
