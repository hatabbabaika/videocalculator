package ru.videocalculator.main;

public class VideoFlow {
	
	private int x = 0;
	private int y = 0;

	private int framerate = 24;
	
	private int colorDepth = 10;

	public VideoFlow() {
	}

	public VideoFlow(int x, int y) {
		this.setSize(x, y);
	}
	
	public int getWidth() {
		return this.x;
	}
	
	public int getHeight() {
		return this.y;
	}
	
	public MemoryView getBitrate() {
		return new MemoryView(this.x)
				.multiply(this.y)
				.multiply(this.colorDepth)
				.multiply(this.framerate);
	}
	
	public VideoFlow setSize(int x, int y) {
		this.x = x;
		this.y = y;
		return this;
	}

	public VideoFlow setFramerate(int i) {
		this.framerate = i;
		return this;
	}
	
	public VideoFlow setColorDepth(int i) {
		this.colorDepth = i;
		return this;
	}

	public String toString() {
		return String.format("%dx%d", this.x, this.y);
	}
}
